import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employeedetail',
  templateUrl: './employeedetail.component.html',
  styles: [``]
})
export class EmployeedetailComponent implements OnInit {
public errorMessage="";
  public employees=[]
  constructor(private _employeeService:EmployeeService) { }

  ngOnInit(): void {
    this._employeeService.getEmployees().subscribe(data => this.employees=data,err =>this.errorMessage=err);
  }

}
