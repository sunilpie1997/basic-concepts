import { Component, OnInit, Input,Output,EventEmitter } from '@angular/core';
//import { EventEmitter } from 'protractor'-------------->don't import from this

@Component({
  selector: 'app-component-interaction',
  template: `
  <div>
  <h1>{{new_message}}</h1>
<button (click)="fireEvent()">emit event</button>
  </div>
  
  `,
  styles: [``]
})
export class ComponentInteractionComponent implements OnInit {

  @Input('message') public new_message;
  @Output() public childEvent=new EventEmitter()
  constructor() { }

 
  ngOnInit(): void {
  }
  fireEvent(){
    this.childEvent.emit("hey this event is emitted from child to parent component")
  }

}
