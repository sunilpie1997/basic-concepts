import { Injectable } from '@angular/core';
import {HttpClientModule, HttpErrorResponse} from '@angular/common/http';
import {HttpClient} from '@angular/common/http';

import { IEmployee } from 'IEmployee';
import { Observable } from 'rxjs';//library for observable
//import 'rxjs/add/observable/throw';
//import  'rxjs/add/operator/catch';//throws error
import {catchError,tap} from 'rxjs/operators';
import {throwError} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  public url:string ="./assets/employee1.json"
  
  constructor(private http:HttpClient) { }//dependency injection instance of HttpClient type is provided

/*public getEmployees(){

return [
{'name':'sunil','age':30,'department':"developer"},
{"name":"raju",'age':31,"department":"sales and marketing"},
{'name':"batari",'age':32,'department':"HR department"}
];
}

 */
getEmployees():Observable<IEmployee[]>{//data is converted to IEmployee array type 
return this.http.get<IEmployee[]>(this.url).pipe(
  catchError((error:HttpErrorResponse) => {throw("error:  "+error.message)})//throws error object
  
);


}



}
