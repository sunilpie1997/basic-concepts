import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  template:`
    <div>
   {{greetUser()}}
   <div>
   {{siteUrl}}
   </div>
   </div>
   <input type="text" value="sunil" disabled="{{x}}">
   <input type="text" value="sunil" [disabled]="isDisabled">

   <div [ngClass]="messageClass">
   <h1> class binding </h1>
   
   </div>
   <div>
<h1 [style.color]="!hasError?'red':'green'">style binding</h1>
<h3 [style.color]="yellowcolor">style binding different way</h3>
<h2 [ngStyle]="multipleStyles">style binding using ngStyle directive</h2>

   </div>


   <div>
<button (click)="onClick()">Event Binding</button>
{{greeting}}

<div>
<button (click)="captureEvent($event)">Capturing and logging Event</button>
<button (click)="y='koi aur kaam nahi hai kya'">Capturing and logging Event part 2</button>
{{y}}

</div>

<div>
<input type="text" #myInput >
<button (click)="OnClick2(myInput)">Template reference Variable</button>
</div>
<br>
<div>
ngModel directive

<input type="text" [(ngModel)]="username">
{{username}}
</div>

<br>

<div *ngIf="displayName; then thenBlock; else elseBlock"></div>
<ng-template #thenBlock>
<h1>IF DIRECTIVE</h1>
</ng-template>

<ng-template #elseBlock>
<h2>Name is hidden</h2>
</ng-template>

<br>

<div [ngSwitch]="color">
<h1>Switch Directive</h1>
<h1 *ngSwitchCase="'red'">Red is chosen</h1>
<h1 *ngSwitchCase="'green'">green is chosen</h1>
<h1 *ngSwitchCase="'blue'">blue  is chosen</h1>
<h1 *ngSwitchDefaultCase>pick color</h1>

</div>
<br>
For Directive
<div *ngFor="let color of colors;index as i">

<h1>{{color}}</h1>


</div>
   </div>
          `,
  styles: [`
  <style>
  .text-success{
    color:green
  }
  .text-danger{
   color:red
  }

  .text-special{
    font-style:italic

  }
  </style>
  `]
})
export class TestComponent implements OnInit {
  public username=""
  public name="Sunil Prajapat"
  public siteUrl=window.location.href//javascript global variables can be used only inside class
  x=false 
   isDisabled=false
   hasError=true
   isSpecial=true
   yellowcolor="yellow"
   greeting=""
   y=""
   displayName=true
   color="blue"
   colors=['red','blue','green']
   
  constructor() { }

  ngOnInit(): void {
  }

  onClick(){
    this.greeting="welcome sunil"
    //data binding
  }
  public greetUser(){
    return "hello"+" "+this.name
  }

  public messageClass={

    "text-success":!this.hasError,
    "text-danger":this.hasError,
    "text-succcess":this.isSpecial,
  }
  public multipleStyles={
    colour:"blue",
    fontSize:"30px"
  }
  captureEvent(event){
    console.log(event)
  }
  OnClick2(value)
  {
    value.value="sunil"
  }
}
